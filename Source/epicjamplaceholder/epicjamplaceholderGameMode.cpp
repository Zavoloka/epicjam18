// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "epicjamplaceholderGameMode.h"
#include "epicjamplaceholderHUD.h"
#include "epicjamplaceholderCharacter.h"
#include "UObject/ConstructorHelpers.h"

AepicjamplaceholderGameMode::AepicjamplaceholderGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AepicjamplaceholderHUD::StaticClass();
}
